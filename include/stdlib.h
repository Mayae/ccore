/**
* This file has no copyright assigned and is placed in the Public Domain.
* This file is part of the w64 mingw-runtime package.
* No warranty is given; refer to the file DISCLAIMER within this package.
*/

#ifndef _INC_STDLIB
#define _INC_STDLIB

#include <limits.h>
#include <stddef.h>

#define __CRT_NOT_IMPLEMENTED(name) void name (...) = delete;

#ifdef __cplusplus
extern "C" {
#endif

#ifndef NULL
#ifdef __cplusplus
#define NULL 0
#else
#define NULL ((void *)0)
#endif
#endif

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

#ifndef _ONEXIT_T_DEFINED
#define _ONEXIT_T_DEFINED

	typedef int(__cdecl *_onexit_t)(void);

#ifndef NO_OLDNAMES
#define onexit_t _onexit_t
#endif
#endif

#ifndef _DIV_T_DEFINED
#define _DIV_T_DEFINED

	typedef struct _div_t {
		int quot;
		int rem;
	} div_t;

	typedef struct _ldiv_t {
		long quot;
		long rem;
	} ldiv_t;

	typedef struct 
	{ 
		long long quot;
		long long rem;
	} lldiv_t;

#endif

#define __CRT_CALLC __cdecl

// string conversions

double __CRT_CALLC atof(const char * s);
int __CRT_CALLC atoi(const char * s);
long __CRT_CALLC atol(const char * s);
long long  __CRT_CALLC atoll(const char * s);

double __CRT_CALLC strtod(const char *s, char **endptr);
float __CRT_CALLC strtof(const char *s, char **endptr);
long __CRT_CALLC strtol(const char *s, char **endptr, int radix);
long double __CRT_CALLC strtold(const char * s, char ** endptr);
long long  __CRT_CALLC strtoll(const char* s, char** endptr, int radix);
unsigned long __CRT_CALLC strtoul(const char * s, char ** endptr, int radix);
unsigned long long  __CRT_CALLC strtoull(const char* a, char** endptr, int radix);

// rand

#define RAND_MAX 0x7fff

void __CRT_CALLC srand(unsigned int seed);
int __CRT_CALLC rand(void);

// exit functions

int atexit(void(*func)(void));


// int arithmetic
int __CRT_CALLC abs(int _X);
long __CRT_CALLC labs(long _X);
inline long long __CRT_CALLC llabs(long long _j) { return (_j >= 0 ? _j : -_j); }

div_t __CRT_CALLC div(int _Numerator, int _Denominator);
ldiv_t __cdecl ldiv(long _Numerator, long _Denominator);
lldiv_t __cdecl lldiv(long long _Numerator, long long _Denominator);

// unsupported
__CRT_NOT_IMPLEMENTED(malloc);
__CRT_NOT_IMPLEMENTED(calloc);
__CRT_NOT_IMPLEMENTED(realloc);
__CRT_NOT_IMPLEMENTED(free);
__CRT_NOT_IMPLEMENTED(exit);
__CRT_NOT_IMPLEMENTED(_exit);
__CRT_NOT_IMPLEMENTED(_Exit);
__CRT_NOT_IMPLEMENTED(getenv);
__CRT_NOT_IMPLEMENTED(system);
__CRT_NOT_IMPLEMENTED(bsearch);
__CRT_NOT_IMPLEMENTED(qsort);
__CRT_NOT_IMPLEMENTED(mblen);
__CRT_NOT_IMPLEMENTED(mbtowc);
__CRT_NOT_IMPLEMENTED(wctomb);
__CRT_NOT_IMPLEMENTED(mbstowcs);
__CRT_NOT_IMPLEMENTED(wcstombs);
void abort();

#ifdef __cplusplus
}
#endif

#pragma pack(pop)

#endif
