/*===---- math.h - Standard header for sized integer types --------------===*\
 *
 * Copyright (c) 2017 Janus Lynggaard Thorborg
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
\*===----------------------------------------------------------------------===*/

#ifndef CPPAPE_MATHLIB_H
#define CPPAPE_MATHLIB_H

#include <stdint.h>
#include <float.h>
//#define HAS_F_VERSIONS


#ifdef HAS_F_VERSIONS

#define MATH_FUNC1(name) \
	extern "C" double name(double x1); \
	extern "C" float name ## f(float x1); \
	inline float name(float x) { return name ## f(x); }

#define MATH_FUNC2(name) \
	extern "C" double name(double x1, double y2); \
	extern "C" float name ## f(float x1, float y2); \
	inline float name(float x, float y) { return name ## f(x, y); }

#else

#define MATH_FUNC1(name) \
	extern "C" { float name ## f (float x); double name(double x); long double name ## l (long double x); }

#define MATH_FUNC2(name) \
	extern "C" { float name ## f (float x, float y); double name(double x, double y); long double name ## l (long double x, long double y); }

#define MATH_FUNC3(name) \
	extern "C" { float name ## f (float x, float y, float z); double name(double x, double y, double z); long double name ## l (long double x, long double y, long double z); }

#endif

#define MATH_TYPE1(name, type) \
	extern "C" { float name ## f (type x); double name(type x); long double name ## l (type x); }

#define MATH_TYPE2(name, type) \
	extern "C" { float name ## f (float x, type y); double name(double x, type y); long double name ## l (long double x, type y); }

#define MATH_TYPE3(name, type) \
	extern "C" { float name ## f (float x, float y, type z); double name(double x, double y, type z); long double name ## l (long double x, long double y, type z); }

#define MATH_RET1(name, type) \
	extern "C" { type name ## f (float x); type name(double x); type name ## l (long double x); }

#define MATH_INTRETPERM1(name) \
	MATH_FUNC1(name); \
	MATH_RET1(l ## name, long); \
	MATH_RET1(ll ## name, long long);

#define MATH_TRIG(name) \
	MATH_FUNC1(name); \
	MATH_FUNC1(a ## name); \
	MATH_FUNC1(name ## h); \
	MATH_FUNC1(a ## name ## h);

// common
extern "C"
{
	int abs(int n);
	long labs(long n);
	long long llabs(long long n);
}

// basic
MATH_FUNC1(fabs);
MATH_FUNC2(fmod);
MATH_FUNC2(fmin);
MATH_FUNC2(fmax);
MATH_FUNC2(remainder);
MATH_FUNC2(copysign);
MATH_FUNC3(fma);
MATH_FUNC2(fdim);
MATH_TYPE1(nan, const char*);
MATH_TYPE3(remquo, int*);

// exponential
MATH_FUNC1(exp);
MATH_FUNC1(exp2);
MATH_FUNC1(expm1);
MATH_FUNC1(log);
MATH_FUNC1(log10);
MATH_FUNC1(log2);
MATH_FUNC1(log1p);

// power
MATH_FUNC1(sqrt);
MATH_FUNC2(pow);
MATH_FUNC2(hypot);
MATH_FUNC1(cbrt);

// trigonometry, hyperbolic
MATH_TRIG(sin);
MATH_TRIG(cos);
MATH_TRIG(tan);
MATH_FUNC2(atan2);

// error gamma
MATH_FUNC1(erf);
MATH_FUNC1(erfc);
MATH_FUNC1(tgamma);
MATH_FUNC1(lgamma);

// rounding
MATH_FUNC1(ceil);
MATH_FUNC1(floor);
MATH_FUNC1(trunc);
MATH_INTRETPERM1(round);
MATH_INTRETPERM1(rint);
MATH_FUNC1(nearbyint);

// exponent modifiers
MATH_TYPE2(frexp, int*);
MATH_TYPE2(ldexp, int);
MATH_TYPE2(scalbn, int);
MATH_TYPE2(scalbln, long);
MATH_FUNC2(nextafter);
MATH_TYPE2(nexttoward, long double);
MATH_RET1(ilogb, int);
MATH_FUNC1(logb);
MATH_FUNC2(copysign);

extern "C"
{
	#define M_E 2.71828182845904523536
	#define M_LOG2E 1.44269504088896340736
	#define M_LOG10E 0.434294481903251827651
	#define M_LN2 0.693147180559945309417
	#define M_LN10 2.30258509299404568402
	#define M_PI 3.14159265358979323846
	#define M_PI_2 1.57079632679489661923
	#define M_PI_4 0.785398163397448309616
	#define M_1_PI 0.318309886183790671538
	#define M_2_PI 0.636619772367581343076
	#define M_2_SQRTPI 1.12837916709551257390
	#define M_SQRT2 1.41421356237309504880
	#define M_SQRT1_2 0.707106781186547524401

	double j0(double _X);
	double j1(double _X);
	double jn(int _X, double _Y);
	double y0(double _X);
	double y1(double _X);
	double yn(int _X, double _Y);


	float modff(float arg, float* iptr);
	double modf(double arg, double* iptr);
	long double modfl(long double arg, long double* iptr);

};

// hacks
inline int signbit(float arg)
{
	auto a = (long long*)&arg;
	return *a & 0x80000000;
}

inline int signbit(double arg)
{
	auto a = (long long*)&arg;
	return *a & 0x8000000000000000;
}

inline int signbit(long double arg)
{
	return signbit((double)arg);
}

#define NAN (0.0F/0.0F)
#define HUGE_VALF (1.0F/0.0F)
#define HUGE_VALL (1.0L/0.0L)
#define INFINITY (1.0F/0.0F)


#define FP_NAN		0x0100
#define FP_NORMAL	0x0400
#define FP_INFINITE	(FP_NAN | FP_NORMAL)
#define FP_ZERO		0x4000
#define FP_SUBNORMAL	(FP_NORMAL | FP_ZERO)
/* 0x0200 is signbit mask */

inline int __ccore_fpclassify_double(double x)
{
	union { double f; uint64_t i; } u;
	u.f = x;
	int e = u.i >> 52 & 0x7ff;
	if (!e) return u.i << 1 ? FP_SUBNORMAL : FP_ZERO;
	if (e == 0x7ff) return u.i << 12 ? FP_NAN : FP_INFINITE;
	return FP_NORMAL;
}

inline int __ccore_fpclassify_float(float x)
{
	union { float f; uint32_t i; } u;
	u.f = x;
	int e = u.i >> 23 & 0xff;
	if (!e) return u.i << 1 ? FP_SUBNORMAL : FP_ZERO;
	if (e == 0xff) return u.i << 9 ? FP_NAN : FP_INFINITE;
	return FP_NORMAL;
}

#define fpclassify(x) (sizeof(x) == sizeof(float) ? __ccore_fpclassify_float(x) : __ccore_fpclassify_double(x))

/*
We can't __CRT_INLINE float or double, because we want to ensure truncation
to semantic type before classification.
(A normal long double value might become subnormal when
converted to double, and zero when converted to float.)
*/

/* 7.12.3.2 */
#define isfinite(x) ((fpclassify(x) & FP_NAN) == 0)

/* 7.12.3.3 */
#define isinf(x) (fpclassify(x) == FP_INFINITE)

/* 7.12.3.4 */
/* We don't need to worry about truncation here:
A NaN stays a NaN. */
#define isnan(x) (fpclassify(x) == FP_NAN)

/* 7.12.3.5 */
#define isnormal(x) (fpclassify(x) == FP_NORMAL)

#define isgreater(x, y)			__builtin_isgreater(x, y)
#define isgreaterequal(x, y)	__builtin_isgreaterequal(x, y)
#define isless(x, y)			__builtin_isless(x, y)
#define islessequal(x, y)		__builtin_islessequal(x, y)
#define islessgreater(x, y)		__builtin_islessgreater(x, y)
#define isunordered(u, v)		__builtin_isunordered(u, v)

typedef float float_t;
typedef double double_t;

#define FLT_EVAL_METHOD 0 /* float_t and double_t are equivalent to float and double, respectively */

#endif
